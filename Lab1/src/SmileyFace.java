// SmileyFace.java
// 
// ICS 45J : Lab Assignment 1
// 
// Originally coded by Norm Jacobson, September 2006
// Minor modifications introduced by Alex Thornton, June 2009
// Revised and adapted for ICS45J Fall 2012 by Norman Jacobson, August 2012
//
// Updated for ICS45J by Stephen Em, April 18, 2015

// The class representing one smiley face
public class SmileyFace
{
	Face face;
	Eye leftEye, rightEye;
	Smile smile;
	
	/** Constructor for SmileyFace which starts with  
	 *  an empty face, left/right eye, and smile
	 */
	public SmileyFace()
	{
		face 	 = new Face();
		leftEye  = new Eye();
		rightEye = new Eye();
		smile 	 = new Smile();
	}
	
	/** Constructor to return a copy of given SmileyFace
	 * 
	 * @param original the SmileyFace to copy
	 */
	public SmileyFace(SmileyFace original)
	{
		face	 = new Face(original.getFace());
		leftEye  = new Eye(original.getLeftEye());
		rightEye = new Eye(original.getRightEye());
		smile	 = new Smile(original.getSmile());
	}
	
	/** translate() moves the entire face, including all of its parts,
	by the given horizontal (deltaX) and vertical (deltaY)
	distances. Positive values move the figure right and down; 
	negative ones up and left
	 * 
	 * @param deltaX  the horizontal distance to move the SmileyFace
	 * @param deltaY  the vertical distance to move the SmileyFace
	 */
	public void translate(int deltaX, int deltaY)
	{
		face.translate(deltaX, deltaY);
		leftEye.translate(deltaX, deltaY);
		rightEye.translate(deltaX, deltaY);
		smile.translate(deltaX, deltaY);
	}
	
	// Accessors that return each part of a SmileyFace:
	
	/** Gets the current face
	 * @return  the face of type Face
	 */
	public Face getFace()
	{
		return face;
	}
	
	/** Gets the left eye of the SmileyFace
	 * @return the left eye of type Eye
	 */
	public Eye getLeftEye()
	{
		return leftEye;
	}
	
	/** Gets the right eye of the SmileyFace
	 * @return the right eye of type Eye
	 */
	public Eye getRightEye()
	{
		return rightEye;
	}
	
	/** Gets the smile of the SmileyFace
	 * @return the smile of type Smile
	 */
	public Smile getSmile()
	{
		return smile;
	}
	
	// Accessors that return the 'edges' of the
	// smiley - the leftmost, rightmost, topmost,  
	// and bottom-most points
	
	/** Gets the left edge
	 * @return left most coordinate of SmileyFace
	 */
	public int getLeftEdge()
	{
		return face.getCenterX() - (int)face.getXLength()/2;
	}
	
	/** Gets the right edge
	 * @return right most coordinate of SmileyFace
	 */
	public int getRightEdge()
	{
		return face.getCenterX() + (int)face.getXLength()/2;
	}
	
	/** Gets the top edge
	 * @return top most coordinate of SmileyFace
	 */
	public int getTopEdge()
	{
		return face.getCenterY() - (int)face.getYLength()/2;
	}
	
	/** Gets the bottom edge
	 * @return bottom most coordinate of SmileyFace
	 */
	public int getBottomEdge()
	{
		return face.getCenterY() + (int)face.getYLength()/2;

	}
	
}


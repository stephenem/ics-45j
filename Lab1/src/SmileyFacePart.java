// SmileyFacePart.java
// 
// ICS 45J : Lab Assignment 1
// 
// Originally coded by Norm Jacobson, September 2006
// Minor modifications introduced by Alex Thornton, June 2009
// Face part copy constructor added by Norman Jacobson for ICS 21 
// computerUpperLeft() removed; that's now handled by the graphics routines,
// by Norman Jacobson for ICS21 WInter 2011, December 2011
// Updated for ICS45J by Norman Jacobson, August 2012
//
// Updated for ICS45J by Stephen Em, April 18, 2015

// Color objects are needed to represent the color of the face part
import java.awt.Color;

// A SmileyFacePart represents one part of a smiley face (i.e.,
// the face, an eye, or the smile); it has a color, position and size
abstract class SmileyFacePart
{
	Color color;
	int centerX, centerY;
	double xLength, yLength;
	
	/** When we construct an "empty" SmileyFacePart, we set its color to
	*   gray, while leaving as 0 all the values of its other attributes
	*/
	public SmileyFacePart()
	{
		color	= new Color(80, 80, 80);
		centerX = 0;
		centerY = 0;
		xLength = 0;
		yLength = 0;
	}
	
	/** A copy of a face part is a copy of each of its components
	 * @param orig  SmileyFacePart to copy
	 */
	public SmileyFacePart(SmileyFacePart orig)
	{
		color	= orig.getColor();
		centerX = orig.getCenterX();
		centerY = orig.getCenterY();
		xLength = orig.getXLength();
		yLength = orig.getYLength();
	}
	
	/** Uses the appropriate helper methods to set the
	// various attributes of a SmileyFacePart.
	 * 
	 * @param newColor    Color to set SmileyFacePart
	 * @param newCenterX  Center X coordinate of SmileyFacePart
	 * @param newCenterY  Center Y coordinate of SmileyFacePart
	 * @param newXLength  Horizontal length of SmileyFacePart
	 * @param newYLength  Vertical length of SmileyFacePart 	 
	 * */
	public void setAttributes(
		Color newColor, int newCenterX, int newCenterY,
		double newXLength, double newYLength)
	{
		setColor(newColor);
		setCenter(newCenterX, newCenterY);
		setXLength(newXLength);
		setYLength(newYLength);
	}
	
	/** Moves a SmileyFacePart the given distance horizontally
	// (deltaX) and vertically (deltaY). Positive values move right and down;
	// negative values move left and up
	 * @param deltaX  Difference to move X coordinate
	 * @param deltaY  Difference to move Y coordinate
	 */
	public void translate(int deltaX, int deltaY)
	{
		setCenter(getCenterX() + deltaX, getCenterY() + deltaY);
	}
	
	/** Changes the size of a SmileyFacePart by the given factor.
	// For example, if the part is 20 x 20 and the scaleFactor is 3.5,
	// the part's new size should be 70 x70.
	 * 
	 * @param scaleFactor  Factor to change the scale of X and Y length
	 */
	public void scale(double scaleFactor)
	{
		setXLength(getXLength() * scaleFactor);
		setYLength(getYLength() * scaleFactor);
	}
	
	/** Sets the color of a SmileyFacePart
	 * 
	 * @param c  Color of the SmileyFacePart
	 */
	public void setColor(Color c)
	{
		color = c;
	}
	
	/** Sets the center x- and y-coordinates of a SmileyFacePart
	 * @param x  X coordinate of the SmileyFacePart
	 * @param y  Y coordinate of the SmileyFacePart
	 */
	public void setCenter(int x, int y)
	{
		centerX = x;
		centerY = y;
	}
	
	/** Sets the horizontal length of a SmileyFacePart
	 * @param xLen the length according to the x axis
	 */	public void setXLength(double xLen)
	{
		xLength = xLen;
	}
	
	/** Sets the vertical length of a SmileyFacePart
	 * @param yLen the length according to the y axis
	 */
	public void setYLength(double yLen)
	{
		yLength = yLen;
	}
	
	// Accessor methods to return each of the attributes
	// of a SmileyFacePart.
	
	/** Gets the current color of SmileyFacePart.
	 	@return the current color
	 */
	public Color getColor()
	{
		return color;
	}
	
	/** Gets the current center X coordinate.
	 * @return the center x coordinate
	 */
	public int getCenterX()
	{
		return centerX;
	}
	
	/** Gets the current center Y coordinate.
	 * @return the center y coordinate
	 */
	public int getCenterY()
	{
		return centerY;
	}
	
	/** Gets the current X length.
	 * @return the center x length
	 */
	public double getXLength()
	{
		return xLength;
	}
	
	/** Gets the current Y length.
	 * @return the center y length
	 */
	public double getYLength()
	{
		return yLength;
	}
	
}

// Each part of the face is just a SmileyFacePart --
// but we have each face part as a separate class to
// help keep them straight, and in case in future
// we want specific parts to have characteristics
// particular to them.

// Each class as a constructor that builds a
// "default part"; that is, it calls the
// SmileyFacePart no-parameter constructor.
// Each class also has a copy constructor that 
// makes a copy of its kind of object; again, 
// since every part here is a face part, we 
// just invoke SmileyFacePart's copy constructor.

class Face extends SmileyFacePart
{
	public Face()
	{
		super();
	}
	
	public Face(Face orig)
	{
		super(orig);
	}
}

class Eye extends SmileyFacePart
{
	public Eye()
	{
		super();
	}
	
	public Eye(Eye orig)
	{
		super(orig);
	}
}

class Smile extends SmileyFacePart
{
	public Smile()
	{
		super();
	}
	
	public Smile(Smile orig)
	{
		super(orig);
	}
}


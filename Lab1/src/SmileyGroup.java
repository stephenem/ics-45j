// SmileyGroup.java
// 
// ICS 45J : Lab Assignment 1
// 
// Originally coded by Norman Jacobson, September 2006
// Minor modifications introduced by Alex Thornton, June 2009
// Minor modifications by Norman Jacobson for ICS 21 Fall 2009, September 2009
// Revised and adapted by Norman Jacobson for ICS45J Fall 2012, August 2012

// SmileyGroup is going to be specifying (to other methods) the 
// colors the various smiley face parts ought to be, so we need to 
// have the Color library available:
//
// Updated for ICS45J by Stephen Em, April 18, 2015

import java.awt.Color;

// The predefined Colors (they are final Color objects) are
//		BLACK, BLUE, CYAN, GRAY, DARK_GRAY, LIGHT_GRAY, GREEN, MAGENTA,
//		ORANGE, PINK, RED, WHITE, YELLOW

// A SmileyGroup represents a collection of up to three smiley faces,
// each with its own attributes.
public class SmileyGroup
{
	// Provide fields for the three smiley faces that make up the group
	
	// The constructor builds up to three smileys in the group.  You
	// can construct each face either from scratch or as a copy of an
	// existing one, setting the attributes of the various face parts,
	// translating and/or scaling face parts, or translating the
	// entire smiley face, by calling the appropriate methods.
	//
	// See the lab write-up for the kinds of smiley faces we're expecting
	// you to create and the methods we expect you to employ; withint those
	// requirements, the exact characteristics (shape, color, position) of 
	// each smiley is up to you.
	SmileyFace face1, face2, face3;	
	
	public SmileyGroup()
	{
		// Make face 1
		Color lightYellow = new Color(255, 255, 153);
		face1 = new SmileyFace();
		face1.getFace().setAttributes(lightYellow, 100, 100, 150.0, 150.0);
		face1.getLeftEye().setAttributes(java.awt.Color.blue, 
										 face1.getLeftEdge() + face1.getFace().getCenterX()/2, 
										 face1.getTopEdge() + face1.getFace().getCenterX()/2, 
										 20.0, 
										 20.0);
		face1.getRightEye().setAttributes(java.awt.Color.blue, 
										  face1.getRightEdge() - face1.getFace().getCenterX()/2, 
										  face1.getTopEdge() + face1.getFace().getCenterX()/2, 
										  20.0, 
										  20.0);
		face1.getSmile().setAttributes(face1.getLeftEye().getColor(), 
									   face1.getFace().getCenterX(), 
									   face1.getFace().getCenterY() + (int)face1.getFace().getYLength()/4, 
									   50, 15);
		
		// Make face2
		face2 = new SmileyFace(face1);
		face2.getLeftEye().setXLength(30);
		face2.getRightEye().setXLength(30);
		face2.getFace().scale(.8);
		face2.getLeftEye().scale(.5);
		face2.getRightEye().scale(.5);
		face2.getFace().setColor(java.awt.Color.cyan);
		face2.getLeftEye().setColor(java.awt.Color.gray);
		face2.getLeftEye().setYLength(25);
		face2.getRightEye().setColor(java.awt.Color.gray);
		face2.getRightEye().setYLength(25);
		face2.getSmile().setColor(java.awt.Color.white);
		face2.getSmile().translate(0, 10);
		face2.translate(150, 150);
		
		// Make face3
		Color mouth = new Color(128, 0, 0);
		face3 = new SmileyFace(face2);
		face3.translate(150, 150);
		face3.getFace().setColor(java.awt.Color.magenta);
		face3.getLeftEye().setColor(java.awt.Color.black);
		face3.getLeftEye().setXLength(25);
		face3.getLeftEye().setYLength(15);
		face3.getRightEye().setColor(java.awt.Color.black);
		face3.getRightEye().setXLength(face3.getLeftEye().getXLength());
		face3.getRightEye().setYLength(face3.getLeftEye().getYLength());
		face3.getSmile().setColor(mouth);
		face3.getSmile().setCenter(face3.getSmile().getCenterX(),
									face3.getBottomEdge()-20);
	}
	
	/** Obtain the first smiley face of the group
	 * @return The first SmileyFace
	 */
	public SmileyFace getSmiley1()
	{
		return face1;	
	}
	
	/** Obtain the second smiley face of the group
	 * @return The second SmileyFace
	 */
	public SmileyFace getSmiley2()
	{
		return face2;
	}
	
	/** Obtain the third smiley face of the group
	 * @return The third SmileyFace
	 */
	public SmileyFace getSmiley3()
	{
		return face3;	
	}
	
}


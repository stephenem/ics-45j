// BouncingGroup.java
// 
// ICS 45J : Lab Assignment 2
// 
// Completed by: Stephen Em
// UCInetiD:     ems
// ID:           33819914
// 
// A BouncingGroup represents a collection of up to 
// three bouncing smiley faces, each with its own attributes.
// 
// Same as Lab 1, except that the smileys are bouncing smileys
// rather than vanilla smiley faces.

import java.awt.Color;

// The group of three bouncing smileys
public class BouncingGroup
{
	
	// The three animated smiley faces (the fields) that make up the group
	
	// complete
	
	// This constructor builds up to three smileys in the group.  You
	// can construct each face either from scratch or as a copy of an
	// existing bouncing smiley, setting the attributes of the various
	// face parts, translating and/or scaling face parts, or translating 
	// the entire smiley face.
	// 
	// The predefined Colors are
	//		BLACK, BLUE, CYAN, GRAY, DARK_GRAY, LIGHT_GRAY, GREEN, MAGENTA,
	//		ORANGE, PINK, RED, WHITE, YELLOW
	// 
	// The exact characteristics (shape, color, position) of each smiley are
	// up to you, within the requirements given in the lab write-up.
	
	private AnimatedSmiley face1;
	private AnimatedSmiley face2;
	private AnimatedSmiley face3;
	
	public BouncingGroup()
	{
		// complete
		Color lightYellow = new Color(255, 255, 153);
		face1 = new AnimatedSmiley(-10, 0);
		face1.getFace().setAttributes(lightYellow, 100, 100, 150.0, 150.0);
		face1.getLeftEye().setAttributes(java.awt.Color.blue, 
										 face1.getLeftEdge() + face1.getFace().getCenterX()/2, 
										 face1.getTopEdge() + face1.getFace().getCenterX()/2, 
										 20.0, 
										 20.0);
		face1.getRightEye().setAttributes(java.awt.Color.blue, 
										  face1.getRightEdge() - face1.getFace().getCenterX()/2, 
										  face1.getTopEdge() + face1.getFace().getCenterX()/2, 
										  20.0, 
										  20.0);
		face1.getSmile().setAttributes(face1.getLeftEye().getColor(), 
									   face1.getFace().getCenterX(), 
									   face1.getFace().getCenterY() + (int)face1.getFace().getYLength()/4, 
									   50, 15);
		face2 = new AnimatedSmiley(face1);
		face3 = new AnimatedSmiley(0, 0);
	}
	
	// Accessor methods that return each of the smileys in the group
	
	public AnimatedSmiley getSmiley1()
	{
		return face1;
	}
	
	public AnimatedSmiley getSmiley2()
	{
		return face2;
	}
	
	public AnimatedSmiley getSmiley3()
	{
		return face3;
	}
    
}

